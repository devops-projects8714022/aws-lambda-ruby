variable "region" {
  description = "The AWS region to deploy the Lambda function"
  default     = "us-west-2"
}

variable "function_name" {
  description = "The name of the Lambda function"
  default     = "run_ruby_scripts"
}

variable "runtime" {
  description = "The runtime for the Lambda function"
  default     = "ruby2.7"
}

variable "handler" {
  description = "The handler for the Lambda function"
  default     = "handler.main"
}

variable "zip_file" {
  description = "Path to the zip file containing Ruby scripts"
  default     = "ruby_scripts.zip"
}

variable "schedule_expression" {
  description = "Schedule expression for CloudWatch Event Rule"
  default     = "rate(1 day)"
}

variable "ruby_scripts" {
  description = "List of Ruby scripts to be executed"
  type        = list(string)
  default     = ["script1.rb", "script2.rb", "script3.rb"]
}
