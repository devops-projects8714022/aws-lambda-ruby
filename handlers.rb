require 'json'
require 'open3'

def main(event:, context:)
  script1 = ENV['RUBY_SCRIPT_1']
  script2 = ENV['RUBY_SCRIPT_2']
  script3 = ENV['RUBY_SCRIPT_3']

  execute_script(script1)
  execute_script(script2)
  execute_script(script3)

  { statusCode: 200, body: JSON.generate('Scripts executed successfully!') }
end

def execute_script(script)
  stdout, stderr, status = Open3.capture3("ruby #{script}")
  unless status.success?
    raise "Script #{script} failed with error: #{stderr}"
  end
end
