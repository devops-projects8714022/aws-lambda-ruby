provider "aws" {
  region = var.region
}

resource "aws_iam_role" "lambda_role" {
  name = "lambda_basic_execution"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Principal = {
          Service = "lambda.amazonaws.com"
        },
        Effect = "Allow"
        Sid    = ""
      },
    ],
  })
}

resource "aws_iam_policy_attachment" "lambda_policy_attachment" {
  name       = "lambda_policy_attachment"
  roles      = [aws_iam_role.lambda_role.name]
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_lambda_function" "lambda_ruby" {
  function_name = var.function_name
  runtime       = var.runtime
  role          = aws_iam_role.lambda_role.arn
  handler       = var.handler
  filename      = var.zip_file

  source_code_hash = filebase64sha256(var.zip_file)

  environment {
    variables = {
      RUBY_SCRIPT_1 = var.ruby_scripts[0]
      RUBY_SCRIPT_2 = var.ruby_scripts[1]
      RUBY_SCRIPT_3 = var.ruby_scripts[2]
    }
  }
}

resource "aws_cloudwatch_event_rule" "daily" {
  name                = "daily_lambda_trigger"
  description         = "Triggers Lambda function daily"
  schedule_expression = var.schedule_expression
}

resource "aws_cloudwatch_event_target" "lambda_target" {
  rule      = aws_cloudwatch_event_rule.daily.name
  target_id = "lambda_target"
  arn       = aws_lambda_function.lambda_ruby.arn
}

resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_ruby.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.daily.arn
}
